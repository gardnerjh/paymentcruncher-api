var express = require('express');
var utils = require('../utils');
var MongoObjectID = require('mongodb').ObjectID;
var buyerProfileModel = require('../models/buyerProfileModel');

module.exports = (function() {
	var router = express.Router();
	
	// require basic authentication for all routes
	router.use(utils.authenticate);

	// GET /buyerProfile/
	// Get all buyer profiles under current user.
	router.get('/', function(req, res) {
		buyerProfileModel.getByUser(req.user, function(buyerProfiles, errorCode) {
			if (errorCode) {
				res.status(errorCode).send();
			} else {
				res.status(200).send(buyerProfiles);
			}
		});
	});

	// parameter middleware that validates the buyerProfileId before arriving at route
	router.param('buyerProfileId', function(req, res, next, buyerProfileId) {
		// make sure this user has access to this buyerProfile
		var isAccessible = false;
		if (req.user.buyerProfiles) {
			for (var i = 0; i < req.user.buyerProfiles.length; i++) {
				if (MongoObjectID(req.user.buyerProfiles[i]) == buyerProfileId) {
					isAccessible = true;
					break;
				}
			}
		}
		
		if (isAccessible) {
			next();
		} else {
			console.log('User does not have access to the requested buyer profile.');
			res.status(403).send();
		}
	});
	
	// GET /buyerProfile/:buyerProfileId
	// Get buyer profile by id.
	router.get('/:buyerProfileId', function(req, res) {
		buyerProfileModel.getById(req.buyerProfileId, function(buyerProfile, errorCode) {
			if (errorCode) {
				res.status(errorCode).send();
			} else {
				res.status(200).send(buyerProfile);
			}
		});
	});
	
	// POST /buyerProfile/
	// Create new buyer profile under current user.
	//
	// Parameters (Content-Type: application/json):
	//		name (string): Buyer profile nickname.
	// 		interestRate (double): Interest rate in percent, e.g. 4.25.
	// 		downPayment (double): Down payment in dollars, e.g. 40000.
	//		loanYears (integer): Loan term in years, e.g. 30.
	//		propertyTaxIncrease (double): Expected increase of property taxes from previous year in percent, e.g. 2.0.
	//		salesDiscount (double): Expected sales discount from list price, e.g. 3.0.
	//		pmiRate (double): PMI rate used if down payment totals less than 10% of sales price in percent, e.g. 0.45.
	//		yearlyHomeownersInsurance (double): Yearly homeowner's insurance amount in dollars, e.g. 950.
	//		grossYearlyIncome (double): Gross yearly income in dollars, e.g. 100000.
	//		monthlyPaymentIncomeRatio (double): Desired monthly payment percentage of gross income in percent, e.g. 25.
	//		active (boolean): Indicates whether this is the active buyer profile for the current user, e.g. true.
	router.post('/', function(req, res) {
		var buyerProfile = {
			name: req.body.name,
			interestRate: parseFloat(req.body.interestRate),
			downPayment: parseFloat(req.body.downPayment),
			loanYears: req.body.loanYears,
			propertyTaxIncrease: parseFloat(req.body.propertyTaxIncrease),
			salesDiscount: parseFloat(req.body.salesDiscount),
			pmiRate: parseFloat(req.body.pmiRate),
			yearlyHomeownersInsurance: parseFloat(req.body.yearlyHomeownersInsurance),
			grossYearlyIncome: parseFloat(req.body.grossYearlyIncome),
			monthlyPaymentIncomeRatio: parseFloat(req.body.monthlyPaymentIncomeRatio),
			active: (req.body.active === true)
		}
		
		var user = req.user;
		buyerProfileModel.create(buyerProfile, user, function(errorCode) {
			if (errorCode) {
				res.status(errorCode).send();
			} else {
				res.status(201).send(buyerProfile);
			}
		});
	});
	
	// PUT /buyerProfile/
	// Update existing buyer profile.
	//
	// Parameters (Content-Type: application/json):
	//		name (string): Buyer profile nickname.
	// 		interestRate (double): Interest rate in percent, e.g. 4.25.
	// 		downPayment (double): Down payment in dollars, e.g. 40000.
	//		loanYears (integer): Loan term in years, e.g. 30.
	//		propertyTaxIncrease (double): Expected increase of property taxes from previous year in percent, e.g. 2.0.
	//		salesDiscount (double): Expected sales discount from list price, e.g. 3.0.
	//		pmiRate (double): PMI rate used if down payment totals less than 10% of sales price in percent, e.g. 0.45.
	//		homeownersInsuranceAmount (double): Yearly homeowner's insurance amount in dollars, e.g. 950.
	//		grossYearlyIncome (double): Gross yearly income in dollars, e.g. 100000.
	//		monthlyPaymentIncomeRatio (double): Desired monthly payment percentage of gross income in percent, e.g. 25.
	//		active (boolean): Indicates whether this is the active buyer profile for the current user, e.g. true.
	router.put('/:buyerProfileId', function(req, res) {
		var buyerProfile = { _id: req.params.buyerProfileId };
		if (req.body.name !== undefined) buyerProfile.name = req.body.name;
		if (req.body.interestRate !== undefined) buyerProfile.interestRate = req.body.interestRate;
		if (req.body.downPayment !== undefined) buyerProfile.downPayment = req.body.downPayment;
		if (req.body.loanYears !== undefined) buyerProfile.loanYears = req.body.loanYears;
		if (req.body.propertyTaxIncrease !== undefined) buyerProfile.propertyTaxIncrease = req.body.propertyTaxIncrease;
		if (req.body.salesDiscount !== undefined) buyerProfile.salesDiscount = req.body.salesDiscount;
		if (req.body.pmiRate !== undefined) buyerProfile.pmiRate = req.body.pmiRate;
		if (req.body.yearlyHomeownersInsurance !== undefined) buyerProfile.yearlyHomeownersInsurance = req.body.yearlyHomeownersInsurance;
		if (req.body.grossYearlyIncome !== undefined) buyerProfile.grossYearlyIncome = req.body.grossYearlyIncome;
		if (req.body.monthlyPaymentIncomeRatio !== undefined) buyerProfile.monthlyPaymentIncomeRatio = req.body.monthlyPaymentIncomeRatio;
		if (req.body.active !== undefined) buyerProfile.active = (req.body.active === true);
		
		var user = req.user;
		buyerProfileModel.update(buyerProfile, user, function(buyerProfile, errorCode) {
			if (errorCode) {
				res.status(errorCode).send();
			} else {
				res.status(200).send(buyerProfile);
			}
		});
	 });
	
	// DELETE /buyerProfile/:buyerProfileId
	// Delete buyer profile by id.
	router.delete('/:buyerProfileId', function(req, res) {
		buyerProfileModel.deleteById(req.params.buyerProfileId, function(errorCode) {
			if (errorCode) {
				res.status(errorCode).send();
			} else {
				res.status(200).send();
			}
		});
	});
	
	return router;
})();