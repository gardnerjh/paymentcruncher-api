var express = require('express');
var utils = require('../utils');
var MongoObjectID = require('mongodb').ObjectID;
var propertyModel = require('../models/propertyModel');
var buyerProfileModel = require('../models/buyerProfileModel');
var amortizationScheduleModel = require('../models/amortizationScheduleModel');

module.exports = (function() {
	var router = express.Router();
	
	// require basic authentication for all routes
	router.use(utils.authenticate);

	// GET /property/
	// Get all properties under current user.
	router.get('/', function(req, res) {
		propertyModel.getByUser(req.user, function(properties, errorCode) {
			if (errorCode) {
				res.status(errorCode).send();
			} else {
				res.status(200).send(properties);
			}
		});
	});

	// parameter middleware that validates the propertyId before arriving at route
	router.param('propertyId', function(req, res, next, propertyId) {
		// make sure this user has access to this buyerProfile
		var isAccessible = false;
		if (req.user.properties) {
			for (var i = 0; i < req.user.properties.length; i++) {
				if (MongoObjectID(req.user.properties[i].id) == propertyId) {
					isAccessible = true;
					break;
				}
			}
		}
		
		if (isAccessible) {
			next();
		} else {
			console.log('User does not have access to the requested property.');
			res.status(403).send();
		}
	});

	// GET /property/:propertyId
	// Get property by id.
	router.get('/:propertyId', function(req, res) {
		propertyModel.getById(req.params.propertyId, function(property, errorCode) {
			if (errorCode) {
				res.status(errorCode).send();
			} else {
				res.status(200).send(property);
			}
		});
	});

	// GET /property/:propertyId/amortizationSchedule
	// Get amortization schedule by property id.
	router.get('/:propertyId/amortizationSchedule', function(req, res) {
		amortizationScheduleModel.getByPropertyId(req.params.propertyId, function(amortizationSchedule, errorCode) {
			if (errorCode) {
				res.status(errorCode).send();
			} else {
				res.status(200).send(amortizationSchedule);
			}
		});
	});
	
	// POST /property/
	// Create new property under current user.
	//
	// Parameters (Content-Type: application/json):
	// 		name (string): Property nickname.
	// 		address (string): Property street address.
	//		city (string): Property city.
	//		state (string): Property state, in two letter state abbreviation format, e.g. NJ.
	//		zip (string): Property zip code, in five digit or nine digit format, e.g. 07081 or 070812804.
	//		listPrice (double): Price property is listed at on real estate market in dollars, e.g. 425000.
	//		purchasePrice (optional double): Price property was purchased at in dollars, e.g. 415000.
	//		propertyTax (double): Total yearly property tax for property.
	//		miscMonthlyExpenses (double): Any miscellaneous monthly expenses associated with property in dollars, e.g. 200.
	//		notes (string): Any relevant notes about the property.
	//		listingUrl (string): URL of property listing, for reference.
	//		location (object): Object with coordinates and streetView objects containing location data for the property.
	router.post('/', function(req, res) {
		var property = {
			name: req.body.name,
			address: req.body.address,
			city: req.body.city,
			state: req.body.state,
			zip: req.body.zip,
			listPrice: parseFloat(req.body.listPrice),
			purchasePrice: req.body.purchasePrice ? parseFloat(req.body.purchasePrice) : undefined,
			propertyTax: parseFloat(req.body.propertyTax),
			miscMonthlyExpenses: req.body.miscMonthlyExpenses ? parseFloat(req.body.miscMonthlyExpenses) : 0,
			notes: req.body.notes,
			listingUrl: req.body.listingUrl,
			location: req.body.location
		}
		
		propertyModel.create(property, req.user, function(property, errorCode){
			if (errorCode) {
				res.status(errorCode).send();
			} else {
				res.status(201).send(property);
			}
		});
	});
	
	// PUT /property/sortOrder
	// Update property sort order.
	//
	// Parameters (Content-Type: application/json):
	// 		sortOrder (array): Array of property IDs ordered by desired sort order.
	router.put('/sortOrder', function(req, res) {
		propertyModel.updateSortOrder(req.body.sortOrder, function(updatedPropertyArray, errorCode) {
			if (errorCode) {
				res.status(errorCode).send();
			} else {
				res.status(200).send(updatedPropertyArray);
			}
		});
	});
	
	// PUT /property/
	// Update existing property.
	//
	// Parameters (Content-Type: application/json):
	// 		name (string): Property nickname.
	// 		address (string): Property street address.
	//		city (string): Property city.
	//		state (string): Property state, in two letter state abbreviation format, e.g. NJ.
	//		zip (string): Property zip code, in five digit or nine digit format, e.g. 07081 or 070812804.
	//		listPrice (double): Price property is listed at on real estate market in dollars, e.g. 425000.
	//		purchasePrice (optional double): Price property was purchased at in dollars, e.g. 415000.
	//		propertyTax (double): Total yearly property tax for property.
	//		miscMonthlyExpenses (double): Any miscellaneous monthly expenses associated with property in dollars, e.g. 200.
	//		notes (string): Any relevant notes about the property.
	//		listingUrl (string): URL of property listing, for reference.
	//		location (object): Object with coordinates and streetView objects containing location data for the property.
	router.put('/:propertyId', function(req, res) {
		propertyModel.getById(req.params.propertyId, function(property, errorCode) {
			if (errorCode) {
				res.status(errorCode).send();
			} else {
				if (req.body.name !== undefined) property.name = req.body.name;
				if (req.body.address !== undefined) property.address = req.body.address;
				if (req.body.city !== undefined) property.city = req.body.city;
				if (req.body.state !== undefined) property.state = req.body.state;
				if (req.body.zip !== undefined) property.zip = req.body.zip;
				if (req.body.listPrice !== undefined) property.listPrice = parseFloat(req.body.listPrice);
				if (req.body.purchasePrice !== undefined) property.purchasePrice = parseFloat(req.body.purchasePrice);
				if (req.body.propertyTax !== undefined) property.propertyTax = parseFloat(req.body.propertyTax);
				if (req.body.miscMonthlyExpenses !== undefined) property.miscMonthlyExpenses = parseFloat(req.body.miscMonthlyExpenses);
				if (req.body.notes !== undefined) property.notes = req.body.notes;
				if (req.body.listingUrl !== undefined) property.listingUrl = req.body.listingUrl;
				if (req.body.location !== undefined) property.location = req.body.location;
				
				propertyModel.update(property, req.user, function(property, errorCode) {
					if (errorCode) {
						res.status(errorCode).send();
					} else {
						res.status(200).send(property);
					}
				});
			}
		});
	});
	
	// DELETE /property/:propertyId
	// Delete property by id.
	router.delete('/:propertyId', function(req, res) {
		propertyModel.deleteById(req.params.propertyId, function(errorCode) {
			if (errorCode) {
				res.status(errorCode).send();
			} else {
				res.status(200).send();
			}
		});
	});

	return router;
})();