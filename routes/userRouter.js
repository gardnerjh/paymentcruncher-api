var express = require('express');
var userModel = require('../models/userModel');
var buyerProfileModel = require('../models/buyerProfileModel');
var utils = require('../utils');

module.exports = (function() {
	var router = express.Router();

	// GET /user/
	// Get a user
	router.get('/', utils.authenticate, function(req, res) {
		res.status(200).send(req.user);
	});
	
	// GET /user/verifyPassword?password=password
	// Get a user
	router.get('/verifyPassword', utils.authenticate, function(req, res) {
		var passwordMatch = utils.hash(req.query.password, req.user.salt) === req.user.password;
		console.log('Password matches: ' + passwordMatch);
		res.status(200).send({passwordMatch: passwordMatch});
	});
	
	// POST /user/
	//
	// Parameters (Content-Type: application/json):
	// 		email
	// 		password
	router.post('/', function(req, res) {
		var user = {
			email: req.body.email,
			password: req.body.password,
			ip: req.connection.remoteAddress
		};
		
		userModel.create(user, function(loginData, errorCode) {
			if (errorCode) {
				res.status(errorCode).send();
			} else {
				res.status(201).send(loginData);
			}			
		});
	});
	
	// PUT /user/
	//
	// Parameters (Content-Type: application/json):
	//		name
	// 		email
	// 		password
	router.put('/', utils.authenticate, function(req, res) {
		var user = {
			_id: req.user._id,
			name: req.body.name,
			email: req.body.email
		};
		if (req.body.password) user.password = req.body.password;
		
		userModel.save(user, function(savedUser, errorCode) {
			if (errorCode) {
				res.status(errorCode).send();
			} else {
				res.status(201).send(savedUser);
			}			
		});
	});
	
	// GET /user/validateEmail/:email
	// Determine whether a user already exists with the given email address.
	router.get('/validateEmail/:email', function(req, res) {
		userModel.getByEmail(req.params.email, function(user, errorCode) {
			if (errorCode) {
				res.status(500).send();
			} else {
				var userExists = false;
				if (user) userExists = true;
				res.status(200).send({userExists: userExists});
			}
		});
	});
	
	// GET /user/buyerProfile/active
	// Get active buyer profile for current user.
	router.get('/buyerProfile/active', utils.authenticate, function(req, res) {
		buyerProfileModel.getById(req.user.activeBuyerProfile, function(buyerProfile, errorCode) {
			if (errorCode) {
				res.status(errorCode).send();
			} else {
				res.status(200).send(buyerProfile);
			}
		});
	});
	
	return router;
})();