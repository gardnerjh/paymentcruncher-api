var express = require('express');
var userModel = require('../models/userModel');
var utils = require('../utils');

module.exports = (function() {
	var router = express.Router();
	
	// require basic authentication for all routes
	router.use(utils.authenticate);

	// POST /logout/
	// Logs out user from current session.
	router.post('/', function(req, res) {
		userModel.logout(req.user, req.auth_token, function(errorCode) {
			if (errorCode) {
				res.status(errorCode).send();
			} else {
				res.status(200).send();
			}			
		});
	});
	
	return router;
})();