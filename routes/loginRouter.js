var express = require('express');
var userModel = require('../models/userModel');

module.exports = (function() {
	var router = express.Router();

	// POST /login/
	//
	// Parameters (Content-Type: application/json):
	// 		email
	// 		password
	router.post('/', function(req, res) {
		var login = {
			email: req.body.email,
			password: req.body.password,
			ip: req.connection.remoteAddress
		};
		
		userModel.login(login, function(loginData, errorCode) {
			if (errorCode) {
				res.status(errorCode).send();
			} else {
				res.status(200).send(loginData);
			}
		});
	});
	
	return router;
})();