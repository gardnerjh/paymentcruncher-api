var express = require('express');
var bodyParser = require('body-parser');
var config = require('./config');

var app = express();

// Parse application/json requests
app.use(bodyParser.json());

// Parse application/json requests
var logRequests = function(req, res, next) {
	if (req.method.toLowerCase() !== 'options') {
		console.log('Request received: ' + req.method + ' ' + req.originalUrl);
	}
    next();
};
app.use(logRequests);

// Allow AJAX requests from specified domain
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', config.allowAjaxDomain);
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
	res.header('Access-Control-Allow-Headers', 'Content-Type, Auth-Token');
    next();
};
app.use(allowCrossDomain);

// Add Routers
app.use('/user', require('./routes/userRouter'));
app.use('/login', require('./routes/loginRouter'));
app.use('/logout', require('./routes/logoutRouter'));
app.use('/buyerProfile', require('./routes/buyerProfileRouter'));
app.use('/property', require('./routes/propertyRouter'));

var port = config.port;
app.listen(port);
console.log('Listening on port ' + port + '...');