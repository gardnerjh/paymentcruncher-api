var mongoClient = require('mongodb').MongoClient;
var MongoObjectID = require('mongodb').ObjectID;
var utils = require('../utils');
var config = require('../config');
var buyerProfileModel = require('../models/buyerProfileModel');
var amortizationScheduleModel = require('../models/amortizationScheduleModel');

exports.create = function(property, user, callback) {
	mongoClient.connect(config.mongoUrl, function(err, db) {
		if (err || !db) {
			console.log('Could not connect to DB: ' + err);
			callback(null, 500);
		} else {
			buyerProfileModel.getById(user.activeBuyerProfile, function(buyerProfile, errorCode) {
				if (errorCode) {
					callback(null, 500);
				} else {
					crunchNumbers(property, buyerProfile, function(property) {
						db.collection('property').insert(property, function(err, saved) {
							if (err || !saved) {
								console.log('Property not saved: ' + err);
								callback(null, 500);
							} else {
								console.log('Property saved');
								
								if (!user.properties) {
									user.properties = [];
								}
								user.properties.push(property._id);
								db.collection('user').update({ _id: MongoObjectID(user._id) }, { $set: { properties: user.properties } }, function(err, saved) {
									if (err || !saved) {
										console.log('Property not added to user: ' + err);
										callback(null, 500);
									} else {
										console.log('Property added to user');
										callback(property);
									}
								});
							}
						});
					});
				}
			});
		}
	});
};

var getByUser = exports.getByUser = function(user, callback) {
	mongoClient.connect(config.mongoUrl, function(err, db) {
		if (err || !db) {
			callback(null, 500);
		} else {
			db.collection('property').find({ _id: { $in: user.properties } }).toArray(function(err, properties) {
				if (properties) {
					callback(properties);
				} else if (err) {
					console.log('Could not get properties: ' + err);
					callback(null, 500);
				} else {
					callback();
				}
			});
		}
	});
};

exports.getById = function(id, callback) {
	mongoClient.connect(config.mongoUrl, function(err, db) {
		if (err || !db) {
			callback(null, 500);
		} else {
			db.collection('property').findOne({ _id: MongoObjectID(id) }, function(err, property) {
				if (property) {
					callback(property);
				} else if (err) {
					console.log('Could not get property: ' + err);
					callback(null, 500);
				} else {
					callback();
				}
			});
		}
	});
};

var update = exports.update = function(property, user, callback) {
	mongoClient.connect(config.mongoUrl, function(err, db) {
		if (err || !db) {
			console.log('Could not connect to DB: ' + err);
			callback(null, 500);
		} else {
			buyerProfileModel.getById(user.activeBuyerProfile, function(buyerProfile, errorCode) {
				if (errorCode) {
					callback(null, 500);
				} else {
					crunchNumbers(property, buyerProfile, function(property) {
						var propertyId = MongoObjectID(property._id);
						delete property._id;
						
						db.collection('property').findAndModify({ _id: propertyId }, [], { $set: property }, { new: true }, function(err, saved) {
							if (err || !saved) {
								console.log('Property not saved: ' + err);
								callback(null, 500);
							} else {
								console.log('Property saved');
								property._id = propertyId;
								callback(saved.value);
							}
						});
					});
				}
			});
		}
	});
};

exports.updateSortOrder = function(sortOrderArray, callback) {
	mongoClient.connect(config.mongoUrl, function(err, db) {
		if (err || !db) {
			console.log('Could not connect to DB: ' + err);
			callback(null, 500);
		} else {
			var updatedPropertyArray = [];
			for (var i = 0; i < sortOrderArray.length; i++) {
				var setObject = { sortOrder: i + 1 };
				db.collection('property').findAndModify({ _id: MongoObjectID(sortOrderArray[i]) }, [], { $set: setObject }, { new: true }, function(err, saved) {
					if (err || !saved) {
						console.log('Sort order not saved: ' + err);
						callback(null, 500);
					} else {
						console.log('Sort order saved');
						updatedPropertyArray.push(saved.value);
						if (updatedPropertyArray.length === sortOrderArray.length) {
							callback(updatedPropertyArray);
						}
					}
				});
			}
		}
	});
};

exports.deleteById = function(id, callback) {
	mongoClient.connect(config.mongoUrl, function(err, db) {
		if (err || !db) {
			callback(500);
		} else {
			db.collection('property').remove({ _id: new MongoObjectID(id) }, function(err) {
				if (err) {
					console.log('Could not remove property: ' + err);
					callback(500);
				} else {
					db.collection('user').update({}, { $pull: { properties: new MongoObjectID(id) } }, function(err) {
						if (err) {
							console.log('Could not remove property from users: ' + err);
						}
						db.collection('amortizationSchedule').remove({ propertyId: new MongoObjectID(id) }, function(err) {
							if (err) {
								console.log('Could not remove associated amortization schedule: ' + err);
							}
							callback();
						});
					});
				}
			});
		}
	});
};

exports.recrunchNumbersByUser = function(user, buyerProfile, callback) {
	mongoClient.connect(config.mongoUrl, function(err, db) {
		if (err || !db) {
			callback(500);
		} else {
			getByUser(user, function(properties, error) {
				if (error) {
					callback(500);
				} else {
					var propertiesUpdated = 0;
					for (var i = 0; i < properties.length; i++) {
						update(properties[i], user, function(property, error) {
							if (error) {
								console.log('Property "' + property.name + '" not updated: ' + error);
							}
							propertiesUpdated++;
							
							if (propertiesUpdated === properties.length) {
								console.log('Properties updated.');
								callback();
							}
						});
					}
				}
			});
		}
	});
};

var crunchNumbers = exports.crunchNumbers = function(property, buyerProfile, callback) {
	property.trueTaxPercentage = (property.propertyTax / property.listPrice) * 100;
	if (property.purchasePrice) {
		property.salesPrice = property.purchasePrice;
	} else {
		property.salesPrice = property.listPrice * ( 1 - (buyerProfile.salesDiscount / 100));
	}
	property.downPaymentPercentage = (buyerProfile.downPayment / property.salesPrice) * 100;
	property.loanAmount = property.salesPrice - buyerProfile.downPayment;
	property.estimatedYearlyPropertyTax = property.propertyTax * ( 1 + (buyerProfile.propertyTaxIncrease / 100));
	
	property.monthlyMortgage = utils.getMonthlyMortgagePayment(property.loanAmount, buyerProfile.loanYears, buyerProfile.interestRate);
	property.monthlyTaxes = utils.toMoney(property.estimatedYearlyPropertyTax / 12);	
	property.monthlyHomeownersInsurance = utils.toMoney(buyerProfile.yearlyHomeownersInsurance / 12);
	property.totalMonthlyPayment = utils.toMoney(property.monthlyMortgage + property.monthlyTaxes + property.monthlyHomeownersInsurance + property.miscMonthlyExpenses);
	
	property.mortgagePercentage = (property.monthlyMortgage / property.totalMonthlyPayment) * 100;
	
	if (buyerProfile.downPayment / property.salesPrice < 0.2) {
		property.pmi = {
			monthlyPmi: utils.toMoney(property.loanAmount * ((buyerProfile.pmiRate / 100) / 12))
		};
		property.totalMonthlyPayment = utils.toMoney(property.totalMonthlyPayment + property.pmi.monthlyPmi);
	} else {
		property.pmi = undefined;
	}
	
	property.grossIncomePercentage = (property.totalMonthlyPayment / (buyerProfile.grossYearlyIncome / 12)) * 100;
	
	var amortizationSchedule = utils.getAmortizationSchedule(property, buyerProfile);
	amortizationScheduleModel.create(amortizationSchedule, property, function(amortizationSchedule, errorCode) {
		callback(property);
	});
};