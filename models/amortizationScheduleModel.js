var mongoClient = require('mongodb').MongoClient;
var MongoObjectID = require('mongodb').ObjectID;
var config = require('../config');

var create = exports.create = function(amortizationSchedule, property, callback) {
	mongoClient.connect(config.mongoUrl, function(err, db) {
		if (err || !db) {
			console.log('Could not connect to DB: ' + err);
			callback(null, 500);
		} else {
			amortizationSchedule.propertyId = property._id;
			db.collection('amortizationSchedule').update({ propertyId: property._id }, amortizationSchedule, { upsert: true }, function(err, saved) {
				if (err || !saved) {
					console.log('Amortization schedule not saved: ' + err);
					callback(null, 500);
				} else {
					console.log('Amortization schedule saved');
					callback(amortizationSchedule);
				}
			});
		}
	});
};

exports.getByPropertyId = function(propertyId, callback) {
	mongoClient.connect(config.mongoUrl, function(err, db) {
		if (err || !db) {
			callback(null, 500);
		} else {
			db.collection('amortizationSchedule').findOne({ propertyId: MongoObjectID(propertyId) }, function(err, amortizationSchedule) {
				if (amortizationSchedule) {
					callback(amortizationSchedule);
				} else if (err) {
					console.log('Could not get amortization schedule: ' + err);
					callback(null, 500);
				} else {
					// TODO: create amortization schedule for property if it doesn't exist
					callback();
				}
			});
		}
	});
};