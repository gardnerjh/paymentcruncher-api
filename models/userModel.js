var mongoClient = require('mongodb').MongoClient;
var utils = require('../utils');
var config = require('../config');

exports.create = function(user, callback) {
	var salt = utils.generateSalt();
	var token = utils.generateToken();
	var hashedToken = utils.hash(token, utils.tokenSalt);
	
	var userToSave = {
		email: user.email,
		password: utils.hash(user.password, salt),
		salt: salt,
		logins: [
			{
				token: hashedToken, 
				ip: user.ip, 
				timestamp: Date.now()
			}
		]
	}
	
	mongoClient.connect(config.mongoUrl, function(err, db) {
		if (err || !db) {
			console.log('Could not connect to DB: ' + err);
			callback(null, 500);
		} else {
			db.collection('user').update({email: userToSave.email}, userToSave, {upsert: true}, function(err, saved) {
				if (err || !saved) {
					console.log('User not saved: ' + err);
					callback(null, 500);
				} else {
					console.log('User saved');
					callback({
						rawToken: token,
						user: userToSave
					});
				}
			});
		}
	});
};

exports.save = function(user, callback) {
	var userToSave = {
		_id: user._id,
		name: user.name,
		email: user.email,
	}
	if (user.password) {
		userToSave.salt = utils.generateSalt();
		userToSave.password = utils.hash(user.password, userToSave.salt);
	}
	
	mongoClient.connect(config.mongoUrl, function(err, db) {
		if (err || !db) {
			console.log('Could not connect to DB: ' + err);
			callback(null, 500);
		} else {
			db.collection('user').findAndModify({ _id: userToSave._id }, [], { $set: userToSave }, { new: true }, function(err, saved) {
				if (err || !saved) {
					console.log('User not saved: ' + err);
					callback(null, 500);
				} else {
					console.log('User saved');
					callback(saved.value);
				}
			});
		}
	});
};

exports.login = function(login, callback) {
	mongoClient.connect(config.mongoUrl, function(err, db) {
		if (err || !db) {
			callback(null, 500);
		} else {
			db.collection('user').findOne({ email: login.email }, function(err, user) {
				if (err) {
					console.log('Error getting user: ' + err);
					callback(null, 500);
				} else if (!user) {
					console.log('Email address not found.');
					callback(null, 401);
				} else {
					if (utils.verifyHash(login.password, user.salt, user.password)) {
						console.log('Login successful.');
						
						if (!user.logins) {
							user.logins = [];
						} else {
							// remove all other logins at this ip
							for (var i = 0; i < user.logins.length; i++) {
								if (user.logins[i].ip == login.ip) {
									user.logins.splice(i, 1);
								}
							}
						}
						
						// add login to user
						var token = utils.generateToken();
						var hashedToken = utils.hash(token, utils.tokenSalt);
						user.logins.push({token: hashedToken, ip: login.ip, timestamp: Date.now()});
						
						db.collection('user').update({ _id: user._id }, user, function(err, saved) {
							if (err || !saved) {
								console.log('Error adding login to user: ' + err);
								callback(null, 500);
							} else {
								console.log('Login added to user.');
								callback({
									rawToken: token,
									user: user
								});
							}
						});
					} else {
						console.log('Password does not match.');
						callback(null, 401);
					}
				}
			});
		}
	});
};

exports.logout = function(user, token, callback) {
	var index;
	var tokenHash = utils.hash(token, utils.tokenSalt);
	for (var i = 0; i < user.logins.length; i++) {
		if (user.logins[i].token == tokenHash) {
			index = i;
			break;
		}
	}
	
	if (index != null) {
		mongoClient.connect(config.mongoUrl, function(err, db) {
			if (err || !db) {
				callback(500);
			} else {
				user.logins.splice(index, 1);
				db.collection('user').update({ _id: user._id }, user, function(err, user) {
					if (err) {
						console.log('Error removing login from user: ' + err);
						callback(500);
					} else {
						console.log('Token removed.');
						callback();
					}
				});
			}
		});
	} else {
		callback();
	}
};

exports.getByEmail = function(email, callback) {
	mongoClient.connect(config.mongoUrl, function(err, db) {
		if (err || !db) {
			callback(null, 500);
		} else {
			db.collection('user').findOne({ email: email }, function(err, user) {
				if (user) {
					callback(user);
				} else if (err) {
					console.log('Could not get user: ' + err);
					callback(null, 500);
				} else {
					callback();
				}
			});
		}
	});
};