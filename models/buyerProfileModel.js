var mongoClient = require('mongodb').MongoClient;
var MongoObjectID = require('mongodb').ObjectID;
var config = require('../config');
var propertyModel = require('../models/propertyModel');

exports.create = function(buyerProfile, user, callback) {
	mongoClient.connect(config.mongoUrl, function(err, db) {
		if (err || !db) {
			console.log('Could not connect to DB: ' + err);
			callback(500);
		} else {
			// remove active from buyerProfile because we don't want to save it to the buyerCollection document
			var activeBuyerProfile = buyerProfile.active;
			delete buyerProfile.active;
			
			db.collection('buyerProfile').insert(buyerProfile, function(err, saved) {
				if (err || !saved) {
					console.log('Buyer profile not saved: ' + err);
					callback(500);
				} else {
					console.log('Buyer profile saved');
					
					if (!user.buyerProfiles) {
						user.buyerProfiles = [];
					}
					user.buyerProfiles.push(buyerProfile._id);
					if (activeBuyerProfile) {
						user.activeBuyerProfile = buyerProfile._id;
					}
					db.collection('user').update({ _id: user._id }, { $set: { buyerProfiles: user.buyerProfiles, activeBuyerProfile: user.activeBuyerProfile } }, function(err, saved) {
						if (err || !saved) {
							console.log('Buyer profile not added to user: ' + err);
							callback(500);
						} else {
							console.log('Buyer profile added to user');
							callback();
						}
					});
				}
			});
		}
	});
};

exports.getByUser = function(user, callback) {
	mongoClient.connect(config.mongoUrl, function(err, db) {
		if (err || !db) {
			callback(null, 500);
		} else {
			db.collection('buyerProfile').find({ _id: { $in: user.buyerProfiles } }).toArray(function(err, buyerProfiles) {
				if (buyerProfiles) {
					for (var i = 0; i < buyerProfiles.length; i++) {
						if (buyerProfiles[i]._id.toString() === user.activeBuyerProfile.toString()) {
							buyerProfiles[i].active = true;
							break;
						}
					}
					callback(buyerProfiles);
				} else if (err) {
					console.log('Could not get buyer profiles: ' + err);
					callback(null, 500);
				} else {
					callback();
				}
			});
		}
	});
};

exports.getById = function(id, callback) {
	mongoClient.connect(config.mongoUrl, function(err, db) {
		if (err || !db) {
			callback(null, 500);
		} else {
			db.collection('buyerProfile').findOne({ _id: MongoObjectID(id) }, function(err, buyerProfile) {
				if (buyerProfile) {
					callback(buyerProfile);
				} else if (err) {
					console.log('Could not get buyer profile: ' + err);
					callback(null, 500);
				} else {
					callback();
				}
			});
		}
	});
};

exports.update = function(buyerProfile, user, callback) {
	mongoClient.connect(config.mongoUrl, function(err, db) {
		if (err || !db) {
			console.log('Could not connect to DB: ' + err);
			callback(null, 500);
		} else {
			// remove _id from buyerProfile because we don't want to save it to the buyerProfile document
			var buyerProfileId = MongoObjectID(buyerProfile._id)
			delete buyerProfile._id;
			
			// remove active from buyerProfile because we don't want to save it to the buyerProfile document
			var activeBuyerProfile = buyerProfile.active;
			delete buyerProfile.active;
			
			db.collection('buyerProfile').findAndModify({ _id: buyerProfileId }, [], { $set: buyerProfile }, { new: true }, function(err, savedProfile) {
				if (err || !savedProfile) {
					console.log('Buyer profile not saved: ' + err);
					callback(null, 500);
				} else {
					console.log('Buyer profile saved');
					
					if (activeBuyerProfile) {
						db.collection('user').update({ _id: user._id }, { $set: { activeBuyerProfile: savedProfile.value._id } }, function(err, saved) {
							if (err || !saved) {
								console.log('Buyer profile not set as active: ' + err);
								callback(null, 500);
							} else {
								propertyModel.recrunchNumbersByUser(user, savedProfile, function(error) {
									if (error) {
										console.log('User properties not updated: ' + err);
									} else {
										console.log('User properties updated.');
									}
									savedProfile.value.active = activeBuyerProfile;
									callback(savedProfile.value);
								});
							}
						});
					} else {
						callback(savedProfile.value);
					}
				}
			});
		}
	});
};

exports.deleteById = function(id, callback) {
	mongoClient.connect(config.mongoUrl, function(err, db) {
		if (err || !db) {
			callback(500);
		} else {
			db.collection('buyerProfile').remove({ _id: new MongoObjectID(id) }, function(err) {
				if (err) {
					console.log('Could not remove buyer profile: ' + err);
					callback(500);
				} else {
					db.collection('user').update({}, { $pull: { buyerProfiles: new MongoObjectID(id) } }, function(err) {
						if (err) {
							console.log('Could not remove buyer profile from users: ' + err);
							callback(500);
						} else {
							callback();
						}
					});
				}
			});
		}
	});
};