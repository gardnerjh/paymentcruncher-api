var crypto = require('crypto');
var mongoClient = require('mongodb').MongoClient;
var config = require('./config');

var tokenSalt = exports.tokenSalt = '!h0P3tH!$!$g0oD3n0UgH';

var authenticate = exports.authenticate = function(req, res, next) {
	if (req.method.toLowerCase() == 'options') {
		next();
	} else {
		var token = req.headers['auth-token'];
		if (token) {
			mongoClient.connect(config.mongoUrl, function(err, db) {
				if (err || !db) {
					console.log('Could not connect to DB: ' + err);
					res.status(500).send();
				} else {
					var tokenHash = hash(token, tokenSalt);
					db.collection('user').findOne({logins: { $elemMatch: { token: tokenHash }}}, function(err, user) {
						if (user) {
							console.log('Authentication successful.');
							req.user = user;
							req.auth_token = token;
							next();
						} else {
							console.log('Token not found.');
							res.status(401).send();
						}
					});
				}
			});
		} else {
			console.log('Token not supplied.');
			res.status(401).send();
		}
	}
};

var hash = exports.hash = function(plainText, salt) {
	return crypto.createHash('sha256').update(plainText + salt).digest('hex');
};

var generateSalt = exports.generateSalt = function() {
	return generateRandomString(16);
};

var generateToken = exports.generateToken = function() {
	return generateRandomString(16);
};

var generateRandomString = exports.generateRandomString = function(numOfBytes) {
	return crypto.randomBytes(numOfBytes).toString('hex');
};

var verifyHash = exports.verifyHash = function(plainText, salt, cipherText) {
	return hash(plainText, salt) === cipherText;
};

var getMortgageBreakdown = exports.getMortgageBreakdown = function(amount, rate, loanYears, monthNumber) {
	var totalMonths = loanYears * 12;
	var monthlyInterestRate = (rate / 12) / 100;
	var monthlyPayment = toMoney(amount * (monthlyInterestRate / (1 - Math.pow(1 + monthlyInterestRate, -(totalMonths)))));
	var monthlyInterest;
	var monthlyPrincipal;
	for (var j = 1; j <= monthNumber; j++) {
		monthlyInterest = toMoney(amount * monthlyInterestRate);
		monthlyPrincipal = toMoney(monthlyPayment - monthlyInterest);
		amount -= monthlyPrincipal;
	}
	return {principal: monthlyPrincipal, interest: monthlyInterest, payment: monthlyPayment};
};

var getMonthlyMortgagePayment = exports.getMonthlyMortgagePayment = function(loanAmount, loanYears, rate) {
	return (getMortgageBreakdown(loanAmount, rate, loanYears, 1)).payment;
};

var getAmortizationSchedule = exports.getAmortizationSchedule = function(property, buyerProfile) {
	var targetPrincipal = property.salesPrice * 0.2;
	
	var totalPmiToPay = 0;
	var pmiPaymentCount = 0;
	
	var totalEquity = buyerProfile.downPayment;
	var numMonths = buyerProfile.loanYears * 12;
	var amortizationArray = [];
	for (var i = 1; i <= numMonths; i++) {
		var payment = getMortgageBreakdown(property.loanAmount, buyerProfile.interestRate, buyerProfile.loanYears, i);
		var totalPayment = toMoney(payment.principal + payment.interest + property.monthlyTaxes + property.monthlyHomeownersInsurance + property.miscMonthlyExpenses);
		
		totalEquity = toMoney(totalEquity + payment.principal);
		if (totalEquity < targetPrincipal) {
			pmiPaymentCount++;
			totalPmiToPay += property.pmi.monthlyPmi;
			totalPayment += property.pmi.monthlyPmi;
		}
		
		var paymentObject = {
			paymentNumber: i,
			principalPayment: payment.principal,
			interestPayment: payment.interest,
			taxPayment: property.monthlyTaxes,
			homeOwnersInsurancePayment: toMoney(property.monthlyHomeownersInsurance),
			totalPayment: totalPayment,
			totalEquity: totalEquity
		};
		if (totalEquity < targetPrincipal) {
			paymentObject.pmiPayment = property.pmi.monthlyPmi;
		}
		if (property.miscMonthlyExpenses > 0) {
			paymentObject.miscExpenses = property.miscMonthlyExpenses;
		};
		
		amortizationArray.push(paymentObject);
	}
	
	if (pmiPaymentCount > 0) {
		property.pmi.totalPmiToPay = toMoney(totalPmiToPay);
		property.pmi.monthsTo20Percent = pmiPaymentCount;
	}
	
	return { payments: amortizationArray };
}

var toMoney = exports.toMoney = function(number) {
	return roundUp(parseFloat(number), 2);
};

var roundUp = exports.round = function(number, precision) {
	var multiplier = Math.pow(10, precision);
	return Number((Math.round(parseFloat(number) * multiplier) / multiplier).toFixed(precision));
};